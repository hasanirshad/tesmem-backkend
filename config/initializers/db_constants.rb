TITLES = {
    photo: "RESERVED_PHOTOS",
    background: "RESERVED_BACKGROUNDS",
    animation: "RESERVED_ANIMATIONS",
    icon: "RESERVED_ICONS",
    design: "RESERVED_DESIGNS"
}.freeze

DESIGNERS = {
    subCategory: "Recent Design(s)"
}.freeze
