class ApplicationMailer < ActionMailer::Base
  default from: 'support@tesmem.com'
  layout 'mailer'
end
