class PublicCategorySerializer < ActiveModel::Serializer
  attributes :id, :title
end
