FactoryBot.define do
  factory :category do
    title {"MyString"}
    description {"MyText"}
    created_at {"2020-05-03 05:17:12"}
    updated_at {"2020-05-03 05:17:12"}
  end
end
