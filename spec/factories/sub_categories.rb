FactoryBot.define do
  factory :sub_category do
    title {"MyString"}
    description {"MyText"}
    category_id {1}
    created_at {"2020-05-16 08:50:16"}
    updated_at {"2020-05-16 08:50:16"}
  end
end
